package com.citylights.todos.domain.todos.interactor

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.todos.repository.TodoRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test

class GetBufferoosTest {

    private lateinit var getTodos: GetTodos

//    private lateinit var mockThreadExecutor: ThreadExecutor
    private lateinit var mockPostExecutionThread: PostExecutionThread
    private lateinit var mockTodoRepository: TodoRepository

    @Before
    fun setUp() {
//        mockThreadExecutor = mock()
        mockPostExecutionThread = mock()
        mockTodoRepository = mock()
        getTodos = GetTodos(mockTodoRepository,
                mockPostExecutionThread)
    }

    @Test
    fun buildUseCaseObservableCallsRepository() {
        getTodos.buildSingle(null)
        verify(mockTodoRepository).getTodos()
    }

//    @Test
//    fun buildUseCaseObservableCompletes() {
//        stubBufferooRepositoryGetBufferoos(Single.just(BufferooFactory.makeBufferooList(2)))
//        val testObserver = getBufferoos.buildUseCaseObservable(null).test()
//        testObserver.assertComplete()
//    }

//    assertComplete@Test
//    fun buildUseCaseObservableReturnsData() {
//        val bufferoos = BufferooFactory.makeBufferooList(2)
//        stubBufferooRepositoryGetBufferoos(Single.just(bufferoos))
//        val testObserver = getBufferoos.buildUseCaseObservable(null).test()
//        testObserver.assertValue(bufferoos)
//    }
//
//    private fun stubBufferooRepositoryGetBufferoos(single: Single<List<Bufferoo>>) {
//        whenever(mockBufferooRepository.getBufferoos())
//                .thenReturn(single)
//    }

}