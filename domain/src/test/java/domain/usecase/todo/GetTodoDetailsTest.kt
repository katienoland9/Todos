package com.citylights.todos.domain.todos.interactor

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.repository.TodoRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import domain.TodoFactory
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class GetTodosTest {

    private lateinit var getTodos: GetTodos

    private lateinit var mockPostExecutionThread: PostExecutionThread
    private lateinit var mockTodoRepository: TodoRepository

    @Before
    fun setUp() {
        mockPostExecutionThread = mock()
        mockTodoRepository = mock()
        getTodos = GetTodos(mockTodoRepository,
                mockPostExecutionThread)
    }

    @Test
    fun buildSingleCallsRepository() {
        getTodos.buildSingle(null)
        verify(mockTodoRepository).getTodos()
    }

    @Test
    fun buildSingleCompletes() {
        stubTodoRepositoryGetTodos(Single.just(TodoFactory.makeTodoList(2)))
        val testObserver = getTodos.buildSingle().test()
        testObserver.assertComplete()
    }

    @Test
    fun buildSingleReturnsData() {
        val todos = TodoFactory.makeTodoList(2)
        stubTodoRepositoryGetTodos(Single.just(todos))
        val testObserver = getTodos.buildSingle().test()
        testObserver.assertValue(todos)
    }

    private fun stubTodoRepositoryGetTodos(single: Single<List<Todo>>) {
        whenever(mockTodoRepository.getTodos())
                .thenReturn(single)
    }

}
