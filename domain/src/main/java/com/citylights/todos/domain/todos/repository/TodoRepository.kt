package com.citylights.todos.domain.todos.repository

import com.citylights.todos.domain.todos.model.CreateTodoResponse
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.model.TodoDetail
import com.citylights.todos.domain.todos.model.UpdateTodoResponse
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Describes how remote repository will access and manipulate data
 * Implemented by remote layer FeatureRemote classes
 * */
interface TodoRepository {
    fun getTodos(): Single<List<Todo>>

    fun getTodoDetails(todoId: String): Single<TodoDetail>

    fun createTodo(todo: Todo): Single<CreateTodoResponse>

    fun updateTodo(todo: Todo): Single<UpdateTodoResponse>

    fun destroyTodo(todoId: String): Completable
}