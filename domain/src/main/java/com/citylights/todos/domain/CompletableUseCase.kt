package com.citylights.todos.domain

import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers

/**
 * manages subscribing to and unsubscribing from a [Completable]
 * @param <Params> type of the parameters used to create the [Completable]
 *
 * @param postExecutionThread thread used to observe the [Completable] ([UiThread] in our case)
 */
abstract class CompletableUseCase<in Params> constructor(val postExecutionThread: PostExecutionThread) {
    var disposables: CompositeDisposable = CompositeDisposable()

    /**
     * Implemented by subclasses to create the [Completable]
     * @param params parameters used during creation of the [Completable]
     *
     * @return the [Completable] for that specific usecase
     */
    abstract fun buildCompletable(params: Params? = null): Completable

    /**
     * Executes the UseCase and subscribes to its [Completable]
     * store the subscription disposable
     *
     * @param observer observer which will subscribe to the [Completable]
     * @param params parameters used during creation of the [Completable]
     */
    fun execute(observer: DisposableCompletableObserver, params: Params? = null) {
        val subscription = buildCompletable(params)
                .subscribeOn(Schedulers.io())
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(observer)

        disposables.add(subscription)
    }

    /**
     * Dispose of all subscriptions to the [Completable]
     * reset the disposable so the UseCase can be reused
     */
    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
            disposables = CompositeDisposable()
        }
    }
}