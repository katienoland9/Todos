package com.citylights.todos.domain.todos.interactor

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.SingleUseCase
import com.citylights.todos.domain.todos.model.CreateTodoResponse
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.repository.TodoRepository
import io.reactivex.Single

class CreateTodo(val repository: TodoRepository, postExecutionThread: PostExecutionThread) :
        SingleUseCase<CreateTodoResponse, CreateTodo.Params>(postExecutionThread) {

    /**
     * Has the remote repository save a new [Todo]
     *
     * @return a [Single] which will enit the [Todo] that was saved
     * */
    override fun buildSingle(params: Params?): Single<CreateTodoResponse> {
        return params?.todo?.let {
            repository.createTodo(it)
        } ?: Single.error(IllegalArgumentException("A todo is required."))
    }

    data class Params(val todo: Todo)

}