package com.citylights.todos.domain.todos.model

data class TodoDetail(val todo: Todo,
                      val items: List<Item>)