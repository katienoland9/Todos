package com.citylights.todos.domain

import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


/**
 * manages subscribing to and unsubscribing from a [Single]
 * @param <T> type of the item emitted by the [Single]
 * @param <Params> type of the parameters used to create the [Single]
 *
 * @param postExecutionThread thread used to observe the [Single] ([UiThread] in our case)
 */
abstract class SingleUseCase<T, in Params> constructor(private val postExecutionThread: PostExecutionThread) {

    private var disposables = CompositeDisposable()

    /**
     * Implemented by subclasses to create the [Single]
     * @param params parameters used during creation of the [Single]
     *
     * @return the [Single] for that specific usecase
     */
    protected abstract fun buildSingle(params: Params? = null): Single<T>

    /**
     * Executes the UseCase and subscribes to its [Single]
     * store the subscription disposable
     *
     * @param singleObserver observer which will subscribe to the [Single]
     * @param params parameters used during creation of the [Single]
     */
    fun execute(singleObserver: DisposableSingleObserver<T>, params: Params? = null) {
        val subscription = this.buildSingle(params)
                .subscribeOn(Schedulers.io())
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(singleObserver)
        disposables.add(subscription)
    }

    /**
     * Dispose of all subscriptions to the [Single]
     * reset the disposable so the UseCase can be reused
     */
    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
            disposables = CompositeDisposable()
        }
    }
}
