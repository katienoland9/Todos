package com.citylights.todos.domain.todos.interactor

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.SingleUseCase
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.model.TodoDetail
import com.citylights.todos.domain.todos.repository.TodoRepository
import io.reactivex.Single

class GetTodoDetails(val repository: TodoRepository, postExecutionThread: PostExecutionThread) :
        SingleUseCase<TodoDetail, GetTodoDetails.Params>(postExecutionThread) {
    /**
     * Has the remote repository load details for a [Todo]
     *
     * @return a [Single] which will emit the [Todo] that was loaded
     * */
    public override fun buildSingle(params: Params?): Single<TodoDetail> {
        return params?.id?.let {
            repository.getTodoDetails(it)

        } ?: Single.error(IllegalArgumentException("A Todo ID is required"))
    }

    data class Params(val id: String)
}