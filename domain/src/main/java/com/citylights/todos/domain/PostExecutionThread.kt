package com.citylights.todos.domain

import io.reactivex.Scheduler

/**
 * Abstraction over AndroidSchedulers.mainThread()
 * implementation provided in ApplicationModule
 * */
interface PostExecutionThread {
    val scheduler: Scheduler
}