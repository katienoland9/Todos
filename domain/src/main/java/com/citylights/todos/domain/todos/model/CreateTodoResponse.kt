package com.citylights.todos.domain.todos.model

import com.citylights.todos.domain.Result


class CreateTodoResponse : Result<Todo, CreateTodoResponse.ErrorType> {
    constructor(todo: Todo): super(todo)
    constructor(errors: List<ErrorType>): super(errors)

    enum class ErrorType {
        TITLE_UNIQUE,
        TITLE_REQUIRED,
        DESCRIPTION_REQUIRED,
        UNHANDLED,
        NETWORK
    }
}