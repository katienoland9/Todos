package com.citylights.todos.domain.todos.model

data class TodoError(val type: ErrorType = ErrorType.UNHANDLED) {
    enum class ErrorType {
        TITLE_UNIQUE,
        TITLE_REQUIRED,
        DESCRIPTION_REQUIRED,
        UNHANDLED,
        NETWORK
    }
}