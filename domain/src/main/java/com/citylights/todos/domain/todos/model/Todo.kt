package com.citylights.todos.domain.todos.model

import java.text.SimpleDateFormat
import java.util.*

data class Todo(val id: Int = -1,
                val title: String = "",
                val description: String = "",
                val createdBy: Int = -1,
                val createdAt: Date = Date(),
                val updatedAt: Date = Date()) {
    companion object {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    }
}