package com.citylights.todos.domain.todos.interactor

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.SingleUseCase
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.repository.TodoRepository
import io.reactivex.Single

class GetTodos(val todoRepository: TodoRepository, postExecutionThread: PostExecutionThread) :
        SingleUseCase<List<Todo>, Void>(postExecutionThread) {
    /**
     * Has the remote repository load all [Todo]
     *
     * @return a [Single] which will emit the [Todo]s that were loaded
     * */
    public override fun buildSingle(params: Void?): Single<List<Todo>> {
        return todoRepository.getTodos()
    }
}