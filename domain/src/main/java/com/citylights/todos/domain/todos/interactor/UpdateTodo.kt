package com.citylights.todos.domain.todos.interactor

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.SingleUseCase
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.model.UpdateTodoResponse
import com.citylights.todos.domain.todos.repository.TodoRepository
import io.reactivex.Single

class UpdateTodo(val repository: TodoRepository, postExecutionThread: PostExecutionThread) :
        SingleUseCase<UpdateTodoResponse, UpdateTodo.Params>(postExecutionThread) {

    /**
     * Has the remote repository update an existing [Todo]
     *
     * @return a [Single] which will emit the [Todo] that was updated
     * */
    override fun buildSingle(params: Params?): Single<UpdateTodoResponse> {
        return params?.todo?.let {
            repository.updateTodo(it)
        } ?: Single.error(IllegalArgumentException("A todo is required."))
    }

    data class Params(val todo: Todo)


}