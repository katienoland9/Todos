package com.citylights.todos.domain.todos.model

import java.util.*

data class Item(val id: Int = -1,
                val name: String = "",
                val done: Boolean = false,
                val todoId: Int = -1,
                val createdAt: Date = Date(),
                val updatedAt: Date = Date())