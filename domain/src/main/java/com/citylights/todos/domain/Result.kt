package com.citylights.todos.domain


abstract class Result<Success, ErrorType>(val isSuccess: Boolean) {
    var data: Success? = null
        private set(value) {
            if (isSuccess) { field = value }
        }
    var errors: List<ErrorType> = listOf()
        private set

    constructor(successResult: Success): this(true) {
        this.data = successResult
    }
    constructor(errors: List<ErrorType>): this(false) {
        this.errors = errors
    }
}