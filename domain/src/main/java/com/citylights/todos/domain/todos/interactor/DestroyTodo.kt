package com.citylights.todos.domain.todos.interactor

import com.citylights.todos.domain.CompletableUseCase
import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.repository.TodoRepository
import io.reactivex.Completable

class DestroyTodo(val repository: TodoRepository, postExecutionThread: PostExecutionThread) :
        CompletableUseCase<DestroyTodo.Params>(postExecutionThread) {
    /**
     * Has the remote repository destroy a [Todo]
     *
     * @return a [Completable] indicating whether or not the operation succeeded
     * */
    override fun buildCompletable(params: Params?): Completable {
        return params?.todoId?.let {
            repository.destroyTodo(it.toString())
        } ?: Completable.error(IllegalArgumentException("A Todo ID is required"))
    }

    data class Params(val todoId: Int)
}