package com.citylights.todos.domain.todos.model

import com.citylights.todos.domain.Result
import com.citylights.todos.domain.todos.interactor.UpdateTodo

class UpdateTodoResponse : Result<Todo, UpdateTodoResponse.ErrorType> {
    constructor(todo: Todo): super(todo)
    constructor(errors: List<UpdateTodoResponse.ErrorType>): super(errors)

    enum class ErrorType {
        TITLE_UNIQUE,
        TITLE_REQUIRED,
        DESCRIPTION_REQUIRED,
        UNHANDLED,
        NETWORK
    }
}