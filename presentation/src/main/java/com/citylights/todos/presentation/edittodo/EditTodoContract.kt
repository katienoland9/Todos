package com.citylights.todos.presentation.edittodo

import com.citylights.todos.domain.todos.model.UpdateTodoResponse
import com.citylights.todos.presentation.BasePresenter
import com.citylights.todos.presentation.BaseView
import com.citylights.todos.presentation.tododetail.model.TodoDetailViewModel
import com.citylights.todos.presentation.todolist.model.TodoViewModel

interface EditTodoContract {
    interface View : BaseView<Presenter> {
        var todoId: Int

        fun showUpdatesuccess()
        fun displayTodoData(todoDetailViewModel: TodoDetailViewModel)
        fun hideLoadingIndicator()
        fun showLoadingIndicator()
        fun clearErrors()
        fun showTitleError(type: UpdateTodoResponse.ErrorType)
        fun showDescriptionError(type: UpdateTodoResponse.ErrorType)
    }
    interface Presenter : BasePresenter {
        fun saveTodo(todoViewModel: TodoViewModel)
    }
}