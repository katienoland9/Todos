package com.citylights.todos.presentation.tododetail

import com.citylights.todos.domain.todos.interactor.GetTodoDetails
import com.citylights.todos.domain.todos.model.TodoDetail
import com.citylights.todos.presentation.tododetail.model.TodoDetailViewModel
import io.reactivex.observers.DisposableSingleObserver

class TodoDetailPresenter(val view: TodoDetailContract.View,
                          val getTodoDetails: GetTodoDetails) : TodoDetailContract.Presenter {
    override fun start() {
        loadTodoDetails()
    }

    override fun stop() {
        getTodoDetails.dispose()
    }

    fun loadTodoDetails() {
        getTodoDetails.execute(object : DisposableSingleObserver<TodoDetail>() {
            override fun onSuccess(t: TodoDetail) {
                view.showTodoDetails(TodoDetailViewModel(t.todo, t.items))
            }

            override fun onError(e: Throwable) {
                view.showGenericError(e.message)
            }
        }, GetTodoDetails.Params(view.todoId.toString()))
    }
}