package com.citylights.todos.presentation.tododetail.model

import com.citylights.todos.domain.todos.model.Item
import com.citylights.todos.domain.todos.model.Todo.Companion.formatter


data class ItemViewModel(val id: Int = -1,
                         val name: String = "",
                         val done: Boolean = false,
                         val createdAt: String = "",
                         val updatedAt: String = "") {
    /**
     * Domain -> Remote model
     * */
    constructor(item: Item) : this(item.id, item.name, item.done, item.createdAt.toString(), item.updatedAt.toString())

    /**
     * Remote -> Domain model
     * */
    fun toDomain(todoId: Int): Item =
            Item(id = id,
                    name = name,
                    done = done,
                    todoId = todoId,
                    createdAt = formatter.parse(createdAt),
                    updatedAt = formatter.parse(updatedAt))
}
