package com.citylights.todos.presentation.splash

import com.citylights.todos.presentation.BasePresenter
import com.citylights.todos.presentation.BaseView

interface SplashContract {
    interface View : BaseView<Presenter> {
        fun launchTodoListActivity()
    }

    interface Presenter : BasePresenter
}