package com.citylights.todos.presentation.todolist

import com.citylights.todos.domain.todos.interactor.DestroyTodo
import com.citylights.todos.domain.todos.interactor.GetTodos
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.presentation.todolist.model.TodoViewModel
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver

class TodoListPresenter(val view: TodoListContract.View,
                        val getTodos: GetTodos,
                        val destroyTodo: DestroyTodo) : TodoListContract.Presenter {

    var todos: List<TodoViewModel> = arrayListOf()

    override fun start() {
        loadTodos()
    }

    override fun stop() {
        getTodos.dispose()
        destroyTodo.dispose()
    }

    override fun removeItem(todoId: Int) {
        destroyTodo.execute(object : DisposableCompletableObserver() {
            override fun onComplete() {
                todos = todos.filterNot { it.id == todoId }
                view.showTodos(todos)
            }

            override fun onError(e: Throwable) {
                view.showGenericError(e.message)
            }
        }, DestroyTodo.Params(todoId))
    }

    fun loadTodos() {
        getTodos.execute(object : DisposableSingleObserver<List<Todo>>() {
            override fun onError(e: Throwable) {
                view.showGenericError(e.message)
            }

            override fun onSuccess(t: List<Todo>) {
                todos = t.map { TodoViewModel(it) }
                view.hideLoadingIndicator()
                view.showTodos(todos)
            }
        })
    }
}