package com.citylights.todos.presentation.addtodo

import com.citylights.todos.domain.todos.interactor.CreateTodo
import com.citylights.todos.domain.todos.model.CreateTodoResponse
import com.citylights.todos.presentation.todolist.model.TodoViewModel
import io.reactivex.observers.DisposableSingleObserver

class AddTodoPresenter(val view: AddTodoContract.View,
                       val createTodo: CreateTodo) : AddTodoContract.Presenter {
    override fun start() {

    }

    override fun stop() {
        createTodo.dispose()
    }

    override fun saveTodo(todoViewModel: TodoViewModel) {
        view.showLoadingIndicator()
        view.clearErrors()
        createTodo.execute(object: DisposableSingleObserver<CreateTodoResponse>() {
            override fun onError(e: Throwable) {
                view.hideLoadingIndicator()
                view.showGenericError(e.toString())
            }

            override fun onSuccess(response: CreateTodoResponse) {
                view.hideLoadingIndicator()

                if (response.isSuccess) {
                    view.showTodoList()
                } else {
                    response.errors.forEach {
                        when (it) {
                            CreateTodoResponse.ErrorType.DESCRIPTION_REQUIRED -> view.showDescriptionError(it)
                            CreateTodoResponse.ErrorType.TITLE_REQUIRED -> view.showTitleError(it)
                            CreateTodoResponse.ErrorType.TITLE_UNIQUE -> view.showTitleError(it)
                            CreateTodoResponse.ErrorType.NETWORK -> view.showGenericError()
                            CreateTodoResponse.ErrorType.UNHANDLED -> view.showGenericError()
                        }
                    }

                }
            }
        }, CreateTodo.Params(todoViewModel.toDomain()))
    }
}