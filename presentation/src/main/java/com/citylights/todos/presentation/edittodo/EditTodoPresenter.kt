package com.citylights.todos.presentation.edittodo

import com.citylights.todos.domain.todos.interactor.GetTodoDetails
import com.citylights.todos.domain.todos.interactor.UpdateTodo
import com.citylights.todos.domain.todos.model.TodoDetail
import com.citylights.todos.domain.todos.model.UpdateTodoResponse
import com.citylights.todos.presentation.tododetail.model.TodoDetailViewModel
import com.citylights.todos.presentation.todolist.model.TodoViewModel
import io.reactivex.observers.DisposableSingleObserver

class EditTodoPresenter(val view: EditTodoContract.View,
                        val getTodoDetails: GetTodoDetails,
                        val updateTodo: UpdateTodo) : EditTodoContract.Presenter {
    var todoDetails: TodoDetailViewModel? = null
    override fun start() {
        getTodoDetails.execute(object : DisposableSingleObserver<TodoDetail>() {
            override fun onSuccess(t: TodoDetail) {
                todoDetails = TodoDetailViewModel(t.todo, t.items)
                todoDetails?.let { view.displayTodoData(it) }
            }

            override fun onError(e: Throwable) {
                view.showGenericError(e.message)
            }

        }, GetTodoDetails.Params(id = view.todoId.toString()))
    }

    override fun stop() {
        getTodoDetails.dispose()
        updateTodo.dispose()
    }

    override fun saveTodo(todoViewModel: TodoViewModel) {
        view.showLoadingIndicator()
        view.clearErrors()

        updateTodo.execute(object : DisposableSingleObserver<UpdateTodoResponse>() {
            override fun onSuccess(t: UpdateTodoResponse) {
                view.hideLoadingIndicator()
                if (t.isSuccess) {
                    view.showUpdatesuccess()
                } else {
                    t.errors.forEach {
                        when(it) {
                            UpdateTodoResponse.ErrorType.TITLE_REQUIRED -> view.showTitleError(it)
                            UpdateTodoResponse.ErrorType.DESCRIPTION_REQUIRED -> view.showTitleError(it)
                            UpdateTodoResponse.ErrorType.TITLE_UNIQUE -> view.showTitleError(it)
                            UpdateTodoResponse.ErrorType.UNHANDLED -> view.showGenericError()
                            UpdateTodoResponse.ErrorType.NETWORK -> view.showGenericError()
                        }
                    }
                }
            }

            override fun onError(e: Throwable) {
                view.showGenericError(e.message)
            }
        }, UpdateTodo.Params(todo = todoViewModel.toDomain()))
    }
}