package com.citylights.todos.presentation.addtodo

import com.citylights.todos.domain.todos.model.CreateTodoResponse
import com.citylights.todos.presentation.BasePresenter
import com.citylights.todos.presentation.BaseView
import com.citylights.todos.presentation.todolist.model.TodoViewModel

interface AddTodoContract {
    interface View : BaseView<Presenter> {
        fun showTodoList()

        fun hideLoadingIndicator()
        fun showLoadingIndicator()
        fun clearErrors()
        fun showTitleError(type: CreateTodoResponse.ErrorType)
        fun showDescriptionError(type: CreateTodoResponse.ErrorType)
    }

    interface Presenter : BasePresenter {
        fun saveTodo(todoViewModel: TodoViewModel)
    }
}