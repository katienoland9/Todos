package com.citylights.todos.presentation.todolist

import com.citylights.todos.presentation.BasePresenter
import com.citylights.todos.presentation.BaseView
import com.citylights.todos.presentation.todolist.model.TodoViewModel

interface TodoListContract {
    interface View: BaseView<Presenter> {
        fun showTodos(todos: List<TodoViewModel>)
        fun hideLoadingIndicator()
    }
    interface Presenter: BasePresenter {
        fun removeItem(todoId: Int)
    }
}