package com.citylights.todos.presentation.tododetail

import com.citylights.todos.presentation.BasePresenter
import com.citylights.todos.presentation.BaseView
import com.citylights.todos.presentation.tododetail.model.TodoDetailViewModel

interface TodoDetailContract {
    interface View : BaseView<Presenter> {
        var todoId: Int
        fun showTodoDetails(todo: TodoDetailViewModel)
    }

    interface Presenter : BasePresenter
}