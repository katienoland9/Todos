package com.citylights.todos.presentation

/**
 * Base MVP Presenter
 * */
interface BasePresenter {
    fun start()
    fun stop()
}