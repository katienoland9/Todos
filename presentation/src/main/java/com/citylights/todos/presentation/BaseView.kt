package com.citylights.todos.presentation

/**
 * Base MVP View
 * */
interface BaseView<T : BasePresenter> {
    var presenter: T

    fun showGenericError(message: String? = "An error occurred.")

}