package com.citylights.todos.presentation.splash

class SplashPresenter(val view: SplashContract.View) : SplashContract.Presenter {
    override fun start() {
        checkUserAuthentication()
    }

    override fun stop() {
    }

    fun checkUserAuthentication() {
        view.launchTodoListActivity()
    }
}