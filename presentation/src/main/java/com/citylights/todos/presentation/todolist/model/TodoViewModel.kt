package com.citylights.todos.presentation.todolist.model

import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.model.Todo.Companion.formatter
import java.util.*


data class TodoViewModel(val id: Int = -1,
                         val title: String = "",
                         val description: String = "",
                         val createdBy: Int = -1,
                         val createdAt: String = formatter.format(Date()),
                         val updatedAt: String = formatter.format(Date()) ) {
    /**
     * Domain -> Remote model
     * */
    constructor(todo: Todo) : this(todo.id, todo.title, todo.description, todo.createdBy, todo.createdAt.toString(), todo.updatedAt.toString())

    /**
     * Remote -> Domain model
     * */
    fun toDomain(): Todo =
            Todo(id = id,
                    title = title,
                    description = description,
                    createdBy = createdBy,
                    createdAt = formatter.parse(createdAt),
                    updatedAt = formatter.parse(updatedAt))
}