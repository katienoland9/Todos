package com.citylights.todos.remote.service

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Singleton for providing configured network services
 * Used in the ui module to build the dependency graph
 * */
object ServiceFactory {
    /**
     * @param isDebug is supplied by the UI module as BuildConfig.DEBUG and used to set logging levels for okhttp
     * */
    fun makeTodoService(isDebug: Boolean): TodoService =
            Retrofit.Builder()
                    .baseUrl("http://10.0.2.2:3000/")
                    .client(makeOkHttpClient(isDebug))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(makeGson()))
                    .build()
                    .create(TodoService::class.java)


    private fun makeOkHttpClient(isDebug: Boolean): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
                .setLevel(if (isDebug) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)

        return OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
    }

    fun makeGson(): Gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .create()
}