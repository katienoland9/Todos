package com.citylights.todos.remote.service

import com.citylights.todos.remote.todos.model.ItemRemoteModel
import com.citylights.todos.remote.todos.model.TodoRemoteModel
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*


interface TodoService {
    /**
     * Todo CRUD
     * */
    @GET("todos")
    fun getTodos(): Single<List<TodoRemoteModel>>

    @GET("todos/{todoId}")
    fun getTodo(@Path("todoId") todoId: String): Single<TodoRemoteModel>

    @POST("todos")
    fun createTodo(@Body todo: TodoRemoteModel): Single<Response<TodoRemoteModel>>

    @PUT("todos/{todoId}")
    fun updateTodo(@Body todo: TodoRemoteModel, @Path("todoId") todoId: String): Single<Response<TodoRemoteModel>>

    @DELETE("todos/{todoId}")
    fun destroyTodo(@Path("todoId") todoId: String): Completable

    /**
     * Item CRUD
     * */
    @GET("todos/{todoId}/items/")
    fun getTodoItems(@Path("todoId") todoId: String): Single<List<ItemRemoteModel>>

    @GET("todos/{todoId}/items/{itemId}")
    fun getTodoItem(@Path("todoId") todoId: String, @Path("itemId") itemId: String): Single<ItemRemoteModel>

}