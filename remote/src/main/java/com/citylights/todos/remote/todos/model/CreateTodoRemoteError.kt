package com.citylights.todos.remote.todos.model

import com.citylights.todos.domain.todos.model.CreateTodoResponse
import com.google.gson.annotations.SerializedName

class CreateTodoRemoteError {

    /**
     * Error response
     * */
    @SerializedName("message")
    var message = ""

    /**
     * Map of expected error strings to domain layer error types
     * */
    val errorMessages = mapOf(
            "Title has already been taken" to CreateTodoResponse.ErrorType.TITLE_UNIQUE,
            "Title can't be blank" to CreateTodoResponse.ErrorType.TITLE_REQUIRED,
            "Description can't be blank" to CreateTodoResponse.ErrorType.DESCRIPTION_REQUIRED)

    /**
     * Remote -> Domain model
     * */
    fun toDomain(): CreateTodoResponse {
        val errors = arrayListOf<CreateTodoResponse.ErrorType>()
        errorMessages.forEach { (msg, errorType) ->
            if (message.contains(msg)) errors.add(errorType)
        }

        if (errors.isEmpty()) errors.add(CreateTodoResponse.ErrorType.UNHANDLED)

        return CreateTodoResponse(errors)

    }
}