package com.citylights.todos.remote.todos.model

import com.citylights.todos.domain.todos.model.Item
import com.google.gson.annotations.SerializedName
import java.util.*

data class ItemRemoteModel(
        @SerializedName("id")
        val id: Int = -1,
        @SerializedName("name")
        val name: String = "",
        @SerializedName("done")
        val done: Boolean = false,
        @SerializedName("todo_id")
        val todoId: Int = -1,
        @SerializedName("created_at")
        val createdAt: Date = Date(),
        @SerializedName("updated_at")
        val updatedAt: Date = Date()) {
    /**
     * Domain -> Remote model
     * */
    constructor(item: Item) : this(item.id, item.name, item.done, item.todoId, item.createdAt, item.updatedAt)

    /**
     * Remote -> Domain model
     * */
    fun toDomain(): Item =
            Item(id = id,
                    name = name,
                    done = done,
                    todoId = todoId,
                    createdAt = createdAt,
                    updatedAt = updatedAt)
}