package com.citylights.todos.remote.todos.model

import com.citylights.todos.domain.todos.model.Todo
import com.google.gson.annotations.SerializedName
import java.util.*

data class TodoRemoteModel(
        @SerializedName("id")
        val id: Int = -1,
        @SerializedName("title")
        val title: String = "",
        @SerializedName("description")
        val description: String = "",
        @SerializedName("created_by")
        val createdBy: Int = -1,
        @SerializedName("created_at")
        val createdAt: Date = Date(),
        @SerializedName("updated_at")
        val updatedAt: Date = Date()) {
        /**
         * Domain -> Remote model
         * */
        constructor(todo: Todo) : this(todo.id, todo.title, todo.description, todo.createdBy, todo.createdAt, todo.updatedAt)

        /**
         * Remote -> Domain model
         * */
        fun toDomain(): Todo =
                Todo(id = id,
                        title = title,
                        description = description,
                        createdBy = createdBy,
                        createdAt = createdAt,
                        updatedAt = updatedAt)
}