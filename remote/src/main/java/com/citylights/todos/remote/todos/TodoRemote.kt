package com.citylights.todos.remote.todos

import com.citylights.todos.domain.todos.model.CreateTodoResponse
import com.citylights.todos.domain.todos.model.Todo
import com.citylights.todos.domain.todos.model.TodoDetail
import com.citylights.todos.domain.todos.model.UpdateTodoResponse
import com.citylights.todos.domain.todos.repository.TodoRepository
import com.citylights.todos.remote.service.ServiceFactory
import com.citylights.todos.remote.service.TodoService
import com.citylights.todos.remote.todos.model.CreateTodoRemoteError
import com.citylights.todos.remote.todos.model.TodoRemoteModel
import com.citylights.todos.remote.todos.model.UpdateTodoRemoteError
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import java.io.IOException


/**
 * Remote implementation of domain layer FeatureRepository interface
 * calls retrofit service methods to access and manipulate server data
 * */
class TodoRemote(val todoService: TodoService) : TodoRepository {
     val gson: Gson = ServiceFactory.makeGson()

    /**
     * loads a list of all [Todo]s from the server
     * */
    override fun getTodos(): Single<List<Todo>> {
        return todoService.getTodos().map { list ->
            list.map { it.toDomain() }
        }
    }

    /**
     * loads a details for a single [Todo] from the server
     * */
    override fun getTodoDetails(todoId: String): Single<TodoDetail> {
        val todo = todoService.getTodo(todoId).map { it.toDomain() }
        val items = todoService.getTodoItems(todoId).map { list ->
            list.map { it.toDomain() }
        }

        return todo.zipWith(items, { todoResult, itemsResult ->
            TodoDetail(todo = todoResult, items = itemsResult)
        })
    }

    /**
     * saves a [Todo]
     * */
    override fun createTodo(todo: Todo): Single<CreateTodoResponse> {
        return todoService.createTodo(TodoRemoteModel(todo))
                .map {
                    // convert success response into domain model
                    if (it.isSuccessful) {
                        it.body()?.toDomain()?.let { CreateTodoResponse(it) }
                    } else {
                        // parse error body into remote model, convert to domain model
                        it.errorBody()?.string()?.let {
                            gson.fromJson(it, CreateTodoRemoteError::class.java).toDomain()
                        }
                    } ?: throw IOException()
                }
    }

    /**
     * update an existing [Todo]
     * */
    override fun updateTodo(todo: Todo): Single<UpdateTodoResponse> {
        return todoService.updateTodo(TodoRemoteModel(todo), todo.id.toString()).map {
            if (it.isSuccessful) {
                it.body()?.toDomain()?.let {
                    UpdateTodoResponse(it)
                }
            } else {
                it.errorBody()?.string()?.let {
                    gson.fromJson(it, UpdateTodoRemoteError::class.java).toDomain()
                }
            } ?: throw IOException()
        }
    }

    /**
     * destroy an existing [Todo]
     * */
    override fun destroyTodo(todoId: String): Completable {
        return todoService.destroyTodo(todoId)
    }
}
