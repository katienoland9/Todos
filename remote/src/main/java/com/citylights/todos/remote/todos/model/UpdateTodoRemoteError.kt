package com.citylights.todos.remote.todos.model

import com.citylights.todos.domain.todos.interactor.UpdateTodo
import com.citylights.todos.domain.todos.model.UpdateTodoResponse
import com.google.gson.annotations.SerializedName

class UpdateTodoRemoteError {

    /**
     * Error response
     * */
    @SerializedName("message")
    var message = ""

    /**
     * Map of expected error strings to domain layer error types
     * */
    val errorMessages = mapOf(
            "Title has already been taken" to UpdateTodoResponse.ErrorType.TITLE_UNIQUE,
            "Title can't be blank" to UpdateTodoResponse.ErrorType.TITLE_REQUIRED,
            "Description can't be blank" to UpdateTodoResponse.ErrorType.DESCRIPTION_REQUIRED)

    /**
     * Remote -> Domain model
     * */
    fun toDomain(): UpdateTodoResponse {
        val errors = arrayListOf<UpdateTodoResponse.ErrorType>()
        errorMessages.forEach { (msg, errorType) ->
            if (message.contains(msg)) errors.add(errorType)
        }

        if (errors.isEmpty()) errors.add(UpdateTodoResponse.ErrorType.UNHANDLED)

        return UpdateTodoResponse(errors)

    }
}