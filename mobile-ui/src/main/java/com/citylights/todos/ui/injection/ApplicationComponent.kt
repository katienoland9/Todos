package com.citylights.todos.ui.injection

import android.app.Application
import com.citylights.todos.ui.TodoApplication
import com.citylights.todos.ui.injection.module.ActivityBindingModule
import com.citylights.todos.ui.injection.module.ApplicationModule
import com.citylights.todos.ui.injection.module.FragmentBindingModule
import com.citylights.todos.ui.injection.module.featuredata.TodoModule
import com.citylights.todos.ui.injection.scopes.PerApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Component containing all application level modules
 * root of dependency graph
 *
 * @param modules the application level modules to include in the component
 */
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    FragmentBindingModule::class,
    ActivityBindingModule::class,
    TodoModule::class])
@PerApplication
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        /**
         * Provides application to the dependency graph
         * */
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: TodoApplication)
}
