package com.citylights.todos.ui.todolist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.citylights.todos.presentation.todolist.model.TodoViewModel
import com.citylights.todos.ui.R
import kotlinx.android.synthetic.main.todo_list_item.view.*

class TodoListAdapter() : RecyclerView.Adapter<TodoListAdapter.ViewHolder>() {
    var todos : List<TodoViewModel> = arrayListOf()
    var clickHandler: (TodoViewModel) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.todo_list_item, parent, false)
        return ViewHolder(itemView)

    }

    override fun getItemCount(): Int = todos.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(todos[position], clickHandler)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(todo: TodoViewModel, clickHandler: (TodoViewModel) -> Unit = {}) {
            itemView.titleTextView.text = todo.title
            itemView.descriptionTextView.text = todo.description
            itemView.setOnClickListener { clickHandler(todo) }
        }
    }
}