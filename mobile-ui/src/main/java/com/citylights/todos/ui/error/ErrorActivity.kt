package com.citylights.todos.ui.error

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.citylights.todos.ui.R
import kotlinx.android.synthetic.main.activity_error.*

class ErrorActivity : AppCompatActivity() {
    companion object {
        val EXTRA_ERROR_MESSAGE = "com.citylights.todos.ui.error.erroractivity.extra_message"
        fun createIntent(context: Context, message: String?): Intent {
            val intent = Intent(context, ErrorActivity::class.java)
            intent.putExtra(EXTRA_ERROR_MESSAGE, message)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error)
        messageTextView.text = intent.getStringExtra(EXTRA_ERROR_MESSAGE) ?: "an error occurred."
    }
}