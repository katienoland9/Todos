package com.citylights.todos.ui.extension

import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity

/**
 * Apply a set of fragment transactions to an [AppCompatActivity]
 * */
fun AppCompatActivity.applyFragmentTransactions(transaction: FragmentTransaction.() -> FragmentTransaction) {
    supportFragmentManager
            .beginTransaction()
            .transaction()
            .commit()
}