package com.citylights.todos.ui.extension

import android.content.Context
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT

fun Context.makeToast(message: String) = Toast.makeText(this, message, LENGTH_SHORT).show()