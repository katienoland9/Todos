package com.citylights.todos.ui.injection.module

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.ui.UiThread
import com.citylights.todos.ui.injection.scopes.PerApplication
import dagger.Module
import dagger.Provides


/**
 * Provides application level dependencies
 * */
@Module
class ApplicationModule {
    @Provides
    @PerApplication
    fun providesPostExecutionThread(): PostExecutionThread = UiThread()
}