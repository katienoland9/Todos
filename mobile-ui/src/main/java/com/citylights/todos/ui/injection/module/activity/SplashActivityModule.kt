package com.citylights.todos.ui.injection.module.activity

import com.citylights.todos.presentation.splash.SplashContract
import com.citylights.todos.presentation.splash.SplashPresenter
import com.citylights.todos.ui.injection.scopes.PerActivity
import com.citylights.todos.ui.splash.SplashActivity
import dagger.Module
import dagger.Provides


@Module
class SplashActivityModule {
    /**
     * View
     * */
    @PerActivity
    @Provides
    fun splashActivityProvider(splashActivity: SplashActivity): SplashContract.View = splashActivity

    /**
     * Presenter
     * */
    @PerActivity
    @Provides
    fun splashPresenterProvider(splashActivity: SplashActivity): SplashContract.Presenter {
        return SplashPresenter(view = splashActivity)
    }
}