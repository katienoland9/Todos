package com.citylights.todos.ui.injection.module.fragment

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.todos.interactor.DestroyTodo
import com.citylights.todos.domain.todos.interactor.GetTodos
import com.citylights.todos.domain.todos.repository.TodoRepository
import com.citylights.todos.presentation.todolist.TodoListContract
import com.citylights.todos.presentation.todolist.TodoListPresenter
import com.citylights.todos.ui.injection.scopes.PerFragment
import com.citylights.todos.ui.todolist.TodoListAdapter
import com.citylights.todos.ui.todolist.TodoListFragment
import dagger.Module
import dagger.Provides

@Module
class TodoListFragmentModule {

    /**
     * View
     * */
    @Provides
    @PerFragment
    fun provideTodoListFragment(todoListFragment: TodoListFragment): TodoListContract.View = todoListFragment


    @Provides
    @PerFragment
    fun provideTodoListAdapter(): TodoListAdapter = TodoListAdapter()

    /**
     * Presenter
     * */
    @Provides
    @PerFragment
    fun providesTodoListPresenter(todoListFragment: TodoListFragment, getTodos: GetTodos, destroyTodo: DestroyTodo): TodoListContract.Presenter {
        return TodoListPresenter(view = todoListFragment, getTodos = getTodos, destroyTodo = destroyTodo)
    }

    /**
     * UseCases
     * */
    @PerFragment
    @Provides
    fun provideDestroyTodo(todoRepository: TodoRepository, postExecutionThread: PostExecutionThread): DestroyTodo {
        return DestroyTodo(repository = todoRepository, postExecutionThread = postExecutionThread)
    }

    @Provides
    @PerFragment
    fun providesGetTodos(todoRepository: TodoRepository, postExecutionThread: PostExecutionThread): GetTodos {
        return GetTodos(todoRepository = todoRepository, postExecutionThread = postExecutionThread)
    }
}