package com.citylights.todos.ui.edittodo

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import com.citylights.todos.ui.BaseActivity

class EditTodoActivity : BaseActivity() {
    companion object {
        val EXTRA_TODO_ID = "com.citylights.todos.ui.edittodo.edittodoactivity.extra_todo_id"
        fun newIntent(todoId: Int, context: Context): Intent {
            val intent = Intent(context, EditTodoActivity::class.java)
            intent.putExtra(EXTRA_TODO_ID, todoId)
            return intent
        }
    }

    override fun createFragment(): Fragment =
            EditTodoFragment.newInstance(intent.getIntExtra(EXTRA_TODO_ID, -1))

}
