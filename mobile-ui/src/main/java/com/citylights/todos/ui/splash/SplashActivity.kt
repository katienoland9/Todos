package com.citylights.todos.ui.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.citylights.todos.presentation.splash.SplashContract
import com.citylights.todos.ui.error.ErrorActivity
import com.citylights.todos.ui.todolist.TodoListActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class SplashActivity : AppCompatActivity(), SplashContract.View {
    @Inject override lateinit var presenter: SplashContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
    }

    override fun launchTodoListActivity() {
        startActivity(Intent(this, TodoListActivity::class.java))
        finish()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun showGenericError(message: String?) {
        startActivity(ErrorActivity.createIntent(this, message))
        finish()
    }
}
