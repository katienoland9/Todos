package com.citylights.todos.ui.edittodo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.citylights.todos.domain.todos.model.UpdateTodoResponse
import com.citylights.todos.presentation.edittodo.EditTodoContract
import com.citylights.todos.presentation.tododetail.model.TodoDetailViewModel
import com.citylights.todos.presentation.todolist.model.TodoViewModel
import com.citylights.todos.ui.BaseFragment
import com.citylights.todos.ui.R
import com.citylights.todos.ui.displayString
import com.citylights.todos.ui.extension.makeToast
import com.citylights.todos.ui.tododetail.TodoDetailActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_edit_todo.*
import javax.inject.Inject

class EditTodoFragment : BaseFragment<EditTodoContract.Presenter>(), EditTodoContract.View {

    @Inject lateinit override var presenter: EditTodoContract.Presenter
    override var todoId: Int = -1

    companion object {
        val ARG_TODO_ID = "com.citylights.todos.ui.edittodo.edittodofragment.arg_todo_id"

        fun newInstance(todoId: Int): EditTodoFragment {
            val args = Bundle()
            val fragment = EditTodoFragment()
            args.putInt(ARG_TODO_ID, todoId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onStop() {
        super.onStop()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        todoId = arguments.getInt(ARG_TODO_ID)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_edit_todo, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        saveButton.setOnClickListener {
            val todo = TodoViewModel(id = todoId,
                    title = titleEditText.text.toString(),
                    description = descriptionEditText.text.toString())
            presenter.saveTodo(todo)
        }
    }

    override fun displayTodoData(todoDetailViewModel: TodoDetailViewModel) {
        titleEditText.setText(todoDetailViewModel.title)
        descriptionEditText.setText(todoDetailViewModel.description)
    }

    override fun showUpdatesuccess() {
        context.makeToast("Successfully updated todo")
        startActivity(TodoDetailActivity.createIntent(context, todoId))
    }

    override fun hideLoadingIndicator() {
        progressBar.visibility = View.GONE
        contentFrame.visibility = View.VISIBLE
    }

    override fun showLoadingIndicator() {
        progressBar.visibility = View.VISIBLE
        contentFrame.visibility = View.GONE
    }

    override fun showTitleError(type: UpdateTodoResponse.ErrorType) {
        titleTextInputLayout.isErrorEnabled = true
        titleTextInputLayout.error = type.displayString()
    }

    override fun showDescriptionError(type: UpdateTodoResponse.ErrorType) {
        descriptionTextInputLayout.isErrorEnabled = true
        descriptionTextInputLayout.error = type.displayString()
    }

    override fun clearErrors() {
        descriptionTextInputLayout.isErrorEnabled = false
        descriptionTextInputLayout.error = ""

        titleTextInputLayout.isErrorEnabled = false
        titleTextInputLayout.error = ""
    }
}