package com.citylights.todos.ui.todolist

import android.support.v4.app.Fragment
import com.citylights.todos.ui.BaseActivity

class TodoListActivity : BaseActivity() {
    override fun createFragment(): Fragment = TodoListFragment.newInstance()
}
