package com.citylights.todos.ui.tododetail

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.citylights.todos.presentation.tododetail.TodoDetailContract
import com.citylights.todos.presentation.tododetail.model.TodoDetailViewModel
import com.citylights.todos.ui.BaseFragment
import com.citylights.todos.ui.R
import com.citylights.todos.ui.edittodo.EditTodoActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.todo_detail_fragment.*
import javax.inject.Inject

class TodoDetailFragment : BaseFragment<TodoDetailContract.Presenter>(), TodoDetailContract.View {

    @Inject override lateinit var presenter: TodoDetailContract.Presenter
    @Inject lateinit var itemListAdapter: ItemListAdapter

    override var todoId: Int = -1
    companion object {

        val ARG_TODO_ID = "com.citylights.todos.ui.tododetail.tododetailfragment.arg_todo_id"

        fun newInstance(todoId: Int): TodoDetailFragment {
            val fragment = TodoDetailFragment()
            val args = Bundle()
            args.putInt(ARG_TODO_ID, todoId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        todoId = arguments.getInt(ARG_TODO_ID)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.todo_detail_fragment, container, false)
    }

    override fun showTodoDetails(todo: TodoDetailViewModel) {
        progressBar.visibility = View.GONE
        contentLayout.visibility = View.VISIBLE

        titleTextView.text = todo.title
        descriptionTextView.text = todo.description
        itemListRecyclerView.layoutManager = LinearLayoutManager(context)
        itemListRecyclerView.adapter = itemListAdapter
        itemListAdapter.items = todo.items
        itemListAdapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
         inflater?.inflate(R.menu.todo_detail_options, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.editMenuItem -> {
                startActivity(EditTodoActivity.newIntent(todoId, context))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}