package com.citylights.todos.ui.injection.module.featuredata

import com.citylights.todos.domain.todos.repository.TodoRepository
import com.citylights.todos.remote.service.ServiceFactory
import com.citylights.todos.remote.service.TodoService
import com.citylights.todos.remote.todos.TodoRemote
import com.citylights.todos.ui.BuildConfig
import com.citylights.todos.ui.injection.scopes.PerApplication
import dagger.Module
import dagger.Provides

/**
 * Provide domain, data, and remote layer classes which might be shared across activities and fragments
 * but all relate to the same feature/data in the inner layers
 * */
@Module
class TodoModule {

    @Provides
    @PerApplication
    fun providesTodoRemoteRepository(todoService: TodoService): TodoRepository {
        return TodoRemote(todoService = todoService)
    }

    @Provides
    @PerApplication
    fun providesTodoService(): TodoService = ServiceFactory.makeTodoService(BuildConfig.DEBUG)
}