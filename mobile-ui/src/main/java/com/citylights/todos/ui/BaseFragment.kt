package com.citylights.todos.ui

import android.support.v4.app.Fragment
import com.citylights.todos.presentation.BasePresenter
import com.citylights.todos.presentation.BaseView
import com.citylights.todos.ui.error.ErrorActivity

/**
 * Base MVP SupportFragment
 * */
abstract class BaseFragment<T: BasePresenter> : BaseView<T>, Fragment() {
    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun showGenericError(message: String?) {
        startActivity(ErrorActivity.createIntent(context, message))
        activity.finish()
    }

}