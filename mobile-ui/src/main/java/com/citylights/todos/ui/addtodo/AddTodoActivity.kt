package com.citylights.todos.ui.addtodo

import android.support.v4.app.Fragment
import com.citylights.todos.ui.BaseActivity

class AddTodoActivity : BaseActivity() {
    override fun createFragment(): Fragment = AddTodoFragment.newInstance()
}
