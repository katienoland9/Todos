package com.citylights.todos.ui.todolist

import android.content.Intent
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.citylights.todos.presentation.todolist.TodoListContract
import com.citylights.todos.presentation.todolist.model.TodoViewModel
import com.citylights.todos.ui.BaseFragment
import com.citylights.todos.ui.R
import com.citylights.todos.ui.addtodo.AddTodoActivity
import com.citylights.todos.ui.tododetail.TodoDetailActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.todo_list_fragment.*
import javax.inject.Inject


class TodoListFragment : BaseFragment<TodoListContract.Presenter>(), TodoListContract.View {

    @Inject override lateinit var presenter: TodoListContract.Presenter
    @Inject lateinit var todoListAdapter: TodoListAdapter

    companion object {
        fun newInstance(): TodoListFragment = TodoListFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.todo_list_fragment, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureRecyclerView()
        addTodoListItemFab.setOnClickListener {
            startActivity(Intent(context, AddTodoActivity::class.java))
        }

    }

    override fun showTodos(todos: List<TodoViewModel>) {
        todoListAdapter.todos = todos
        todoListAdapter.notifyDataSetChanged()
    }

    override fun hideLoadingIndicator() {
        progressBar.visibility = View.GONE
        contentLayout.visibility = View.VISIBLE
        addTodoListItemFab.visibility = View.VISIBLE
    }

    fun configureRecyclerView() {
        todoListAdapter.clickHandler = {
            startActivity(TodoDetailActivity.createIntent(context, it.id))
        }
        todoListRecyclerView.adapter = todoListAdapter
        todoListRecyclerView.layoutManager = LinearLayoutManager(context)

        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.START) {
            override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                todoListAdapter.notifyItemRemoved(position)
                presenter.removeItem(todoListAdapter.todos[position].id)
            }

            override fun onChildDraw(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder,
                                     dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

                val deleteIcon = ContextCompat.getDrawable(context, R.drawable.abc_ic_ab_back_material)

                val background = ColorDrawable()
                background.color = ResourcesCompat.getColor(resources, R.color.error_color_material, context.theme)

                with (viewHolder.itemView) {
                    val deleteIconMargin = ((bottom - top) - deleteIcon.intrinsicHeight) / 2
                    deleteIcon.bounds.top = top + ((bottom - top) - deleteIcon.intrinsicHeight) / 2
                    deleteIcon.bounds.left = right - deleteIconMargin -  deleteIcon.intrinsicWidth
                    deleteIcon.bounds.right = right - deleteIconMargin
                    deleteIcon.bounds.bottom = deleteIcon.bounds.top + deleteIcon.intrinsicHeight

                    background.setBounds(right + dX.toInt(), top, right, bottom)
                }

                background.draw(c)
                deleteIcon.draw(c)

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

            }
        }).attachToRecyclerView(todoListRecyclerView)
    }
}