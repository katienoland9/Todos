package com.citylights.todos.ui.injection.module.fragment

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.todos.interactor.GetTodoDetails
import com.citylights.todos.domain.todos.interactor.UpdateTodo
import com.citylights.todos.domain.todos.repository.TodoRepository
import com.citylights.todos.presentation.edittodo.EditTodoContract
import com.citylights.todos.presentation.edittodo.EditTodoPresenter
import com.citylights.todos.ui.edittodo.EditTodoFragment
import com.citylights.todos.ui.injection.scopes.PerFragment
import dagger.Module
import dagger.Provides

@Module
class EditTodoFragmentModule {
    /**
     * View
     * */
    @Provides
    @PerFragment
    fun providesEditTodoFragment(editTodoFragment: EditTodoFragment): EditTodoContract.View = editTodoFragment


    /**
     * Presenter
     * */
    @Provides
    @PerFragment
    fun providesEditTodoPresenter(editTodoFragment: EditTodoFragment, updateTodo: UpdateTodo, getTodoDetails: GetTodoDetails): EditTodoContract.Presenter {
        return EditTodoPresenter(view = editTodoFragment, updateTodo = updateTodo, getTodoDetails = getTodoDetails)
    }

    /**
     * UseCases
     * */
    @Provides
    @PerFragment
    fun providesGetTodoDetails(todoRepository: TodoRepository, postExecutionThread: PostExecutionThread): GetTodoDetails {
        return GetTodoDetails(postExecutionThread = postExecutionThread, repository = todoRepository)
    }

    @Provides
    @PerFragment
    fun providesUpdateTodo(todoRepository: TodoRepository, postExecutionThread: PostExecutionThread): UpdateTodo {
        return UpdateTodo(postExecutionThread = postExecutionThread, repository = todoRepository)
    }
}