package com.citylights.todos.ui.injection.module

import com.citylights.todos.ui.injection.module.activity.SplashActivityModule
import com.citylights.todos.ui.injection.scopes.PerActivity
import com.citylights.todos.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Binds all activities that require injection to their modules
 * */
@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [SplashActivityModule::class])
    abstract fun splashActivity(): SplashActivity
}