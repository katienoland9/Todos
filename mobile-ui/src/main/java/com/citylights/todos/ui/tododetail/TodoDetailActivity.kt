package com.citylights.todos.ui.tododetail

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import com.citylights.todos.ui.BaseActivity


class TodoDetailActivity : BaseActivity() {

    companion object {
        val EXTRA_TODO_ID = "com.citylights.todos.ui.tododetail.tododetailactivity.extra_todo_id"

        fun createIntent(context: Context, todoId: Int): Intent {
            val intent = Intent(context, TodoDetailActivity::class.java)
            intent.putExtra(EXTRA_TODO_ID, todoId)
            return intent
        }
    }

    override fun createFragment(): Fragment {
        val todoId = intent.getIntExtra(EXTRA_TODO_ID, -1)
        return TodoDetailFragment.newInstance(todoId)
    }
}
