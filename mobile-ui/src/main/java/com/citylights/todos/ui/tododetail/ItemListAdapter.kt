package com.citylights.todos.ui.tododetail

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.citylights.todos.presentation.tododetail.model.ItemViewModel
import com.citylights.todos.ui.R
import kotlinx.android.synthetic.main.item_list_item.view.*

class ItemListAdapter: RecyclerView.Adapter<ItemListAdapter.ViewHolder>() {
    var items: List<ItemViewModel> = arrayListOf()
    val clickHandler: (ItemViewModel) -> Unit = {}

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position].let { item ->
            holder.bind(item)
            holder.itemView.setOnClickListener { clickHandler(item) }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_item, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: ItemViewModel) {
            itemView.nameTextView.text = item.name
            itemView.doneCheckBox.isChecked = item.done
        }
    }
}