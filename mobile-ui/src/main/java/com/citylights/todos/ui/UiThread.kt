package com.citylights.todos.ui

import com.citylights.todos.domain.PostExecutionThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Used to execute actions on the UI thread from non android sdk modules
 * */
class UiThread : PostExecutionThread {
    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}