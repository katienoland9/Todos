package com.citylights.todos.ui

import com.citylights.todos.domain.todos.model.CreateTodoResponse
import com.citylights.todos.domain.todos.model.UpdateTodoResponse


fun CreateTodoResponse.ErrorType.displayString(): String {
   return when (this) {
        CreateTodoResponse.ErrorType.DESCRIPTION_REQUIRED -> "A description is required"
        CreateTodoResponse.ErrorType.TITLE_REQUIRED -> "A title is required"
        CreateTodoResponse.ErrorType.TITLE_UNIQUE -> "Title must be unique"
        CreateTodoResponse.ErrorType.NETWORK -> "A network error occured."
        CreateTodoResponse.ErrorType.UNHANDLED -> "A fatal error occured."
    }
}

fun UpdateTodoResponse.ErrorType.displayString(): String {
    return when (this) {
        UpdateTodoResponse.ErrorType.DESCRIPTION_REQUIRED -> "A description is required"
        UpdateTodoResponse.ErrorType.TITLE_REQUIRED -> "A title is required"
        UpdateTodoResponse.ErrorType.TITLE_UNIQUE -> "Title must be unique"
        UpdateTodoResponse.ErrorType.NETWORK -> "A network error occured."
        UpdateTodoResponse.ErrorType.UNHANDLED -> "A fatal error occured."
    }
}
