package com.citylights.todos.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.citylights.todos.ui.extension.applyFragmentTransactions

/**
 * Base AppCompatActivity
 * hosts a single fragment
 * */
abstract class BaseActivity : AppCompatActivity() {
    abstract fun createFragment(): Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_base)
        applyFragmentTransactions { replace(R.id.contentFrame, createFragment()) }
    }
}