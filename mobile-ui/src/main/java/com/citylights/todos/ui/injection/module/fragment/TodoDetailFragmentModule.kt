package com.citylights.todos.ui.injection.module.fragment

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.todos.interactor.GetTodoDetails
import com.citylights.todos.domain.todos.repository.TodoRepository
import com.citylights.todos.presentation.tododetail.TodoDetailContract
import com.citylights.todos.presentation.tododetail.TodoDetailPresenter
import com.citylights.todos.ui.injection.scopes.PerFragment
import com.citylights.todos.ui.tododetail.ItemListAdapter
import com.citylights.todos.ui.tododetail.TodoDetailFragment
import dagger.Module
import dagger.Provides

@Module
class TodoDetailFragmentModule {
    /**
     * View
     * */
    @PerFragment
    @Provides
    fun provideTodoDetailFragment(todoDetailFragment: TodoDetailFragment): TodoDetailContract.View = todoDetailFragment

    @PerFragment
    @Provides
    fun providesItemListAdapter() = ItemListAdapter()

    /**
     * Presenter
     * */
    @PerFragment
    @Provides
    fun provideTodoDetailPresenter(todoDetailFragment: TodoDetailFragment, getTodoDetails: GetTodoDetails)
            : TodoDetailContract.Presenter {
        return TodoDetailPresenter(view = todoDetailFragment, getTodoDetails = getTodoDetails)
    }

    /**
     * UseCases
     * */
    @PerFragment
    @Provides
    fun provideGetTodoDetails(todoRepository: TodoRepository, postExecutionThread: PostExecutionThread): GetTodoDetails {
        return GetTodoDetails(repository = todoRepository, postExecutionThread = postExecutionThread)
    }


}