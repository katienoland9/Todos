package com.citylights.todos.ui.addtodo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.citylights.todos.domain.todos.model.CreateTodoResponse
import com.citylights.todos.presentation.addtodo.AddTodoContract
import com.citylights.todos.presentation.todolist.model.TodoViewModel
import com.citylights.todos.ui.BaseFragment
import com.citylights.todos.ui.R
import com.citylights.todos.ui.displayString
import com.citylights.todos.ui.error.ErrorActivity
import com.citylights.todos.ui.extension.applyFragmentTransactions
import com.citylights.todos.ui.todolist.TodoListFragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_add_todo.*
import javax.inject.Inject

class AddTodoFragment : BaseFragment<AddTodoContract.Presenter>(), AddTodoContract.View {

    @Inject override lateinit var presenter: AddTodoContract.Presenter

    companion object {
        fun newInstance(): AddTodoFragment = AddTodoFragment()
    }

    override fun showGenericError(message: String?) {
        startActivity(ErrorActivity.createIntent(context, message))
        activity.finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_add_todo, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        saveButton.setOnClickListener {
            val todo = TodoViewModel(title = titleEditText.text.toString(), description = descriptionEditText.text.toString())
            presenter.saveTodo(todo)
        }
    }

    override fun showTodoList() {
        (activity as AppCompatActivity).applyFragmentTransactions { replace(R.id.contentFrame, TodoListFragment.newInstance()) }
    }

    override fun hideLoadingIndicator() {
        progressBar.visibility = View.GONE
        contentFrame.visibility = View.VISIBLE
    }

    override fun showLoadingIndicator() {
        progressBar.visibility = View.VISIBLE
        contentFrame.visibility = View.GONE
    }

    override fun showTitleError(type: CreateTodoResponse.ErrorType) {
        titleTextInputLayout.isErrorEnabled = true
        titleTextInputLayout.error = type.displayString()
    }

    override fun showDescriptionError(type: CreateTodoResponse.ErrorType) {
        descriptionTextInputLayout.isErrorEnabled = true
        descriptionTextInputLayout.error = type.displayString()
    }

    override fun clearErrors() {
        descriptionTextInputLayout.isErrorEnabled = false
        descriptionTextInputLayout.error = ""

        titleTextInputLayout.isErrorEnabled = false
        titleTextInputLayout.error = ""
    }
}