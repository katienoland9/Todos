package com.citylights.todos.ui.injection.module

import com.citylights.todos.ui.addtodo.AddTodoFragment
import com.citylights.todos.ui.edittodo.EditTodoFragment
import com.citylights.todos.ui.injection.module.fragment.AddTodoFragmentModule
import com.citylights.todos.ui.injection.module.fragment.EditTodoFragmentModule
import com.citylights.todos.ui.injection.module.fragment.TodoDetailFragmentModule
import com.citylights.todos.ui.injection.module.fragment.TodoListFragmentModule
import com.citylights.todos.ui.injection.scopes.PerFragment
import com.citylights.todos.ui.tododetail.TodoDetailFragment
import com.citylights.todos.ui.todolist.TodoListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Binds all fragments that require injection to their modules
 * */
@Module
abstract class FragmentBindingModule {
    @PerFragment
    @ContributesAndroidInjector(modules = [TodoListFragmentModule::class])
    abstract fun todoListFragment(): TodoListFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [TodoDetailFragmentModule::class])
    abstract fun todoDetailFragment(): TodoDetailFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [AddTodoFragmentModule::class])
    abstract fun addTodoFragment(): AddTodoFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [EditTodoFragmentModule::class])
    abstract fun editTodoFragment(): EditTodoFragment
}