package com.citylights.todos.ui.injection.module.fragment

import com.citylights.todos.domain.PostExecutionThread
import com.citylights.todos.domain.todos.interactor.CreateTodo
import com.citylights.todos.domain.todos.repository.TodoRepository
import com.citylights.todos.presentation.addtodo.AddTodoContract
import com.citylights.todos.presentation.addtodo.AddTodoPresenter
import com.citylights.todos.ui.addtodo.AddTodoFragment
import com.citylights.todos.ui.injection.scopes.PerFragment
import dagger.Module
import dagger.Provides

@Module
class AddTodoFragmentModule {
    /**
     * View
     * */
    @Provides
    @PerFragment
    fun providesAddTodoFragment(addTodoFragment: AddTodoFragment): AddTodoContract.View =
            addTodoFragment

    /**
     * Presenter
     * */
    @Provides
    @PerFragment
    fun providesAddTodoPresenter(view: AddTodoContract.View, createTodo: CreateTodo): AddTodoContract.Presenter =
            AddTodoPresenter(view, createTodo)

    /**
     * UseCases
     * */
    @Provides
    @PerFragment
    fun providesCreateTodo(todoRepository: TodoRepository, postExecutionThread: PostExecutionThread): CreateTodo =
            CreateTodo(todoRepository, postExecutionThread)

}